var webpack = require('webpack')
  , path = require('path')
  , js_root = path.join(__dirname, 'src')
  , bower_components = path.join(__dirname, 'bower_components')
  , HtmlWebpackPlugin = require('html-webpack-plugin')
  , assign = Object.assign || require('object.assign')

module.exports = function(options) {
  options = options || { production: undefined }

  // The url loader works like the file loader
  // but can return a Data Url if the file is smaller than a limit.
  // 8192 is the file size limit
  return {
    context: js_root
  , entry: [
      'webpack/hot/only-dev-server'
    , path.join(js_root, 'main.js')
    ]
  , output: assign({
      filename: '[name].js'
    }, options.production && { path: 'dist'})
  , resolve: {
      root: [js_root, bower_components]
    , modulesDirectories: ["node_modules", "bower_components"]
    }
  , module: {
      loaders: [
        { test: /\.(js|jsx)$/
        , exclude: /node_modules/
        , loader: 'react-hot!babel-loader'
        , include: js_root }
      , { test: /\.styl/
        , loader: 'style-loader!css-loader!stylus-loader' }
      , { test: /\.css$/
        , loader: 'style-loader!css-loader' }
      , { test: /\.(gif|png|jpg)$/
        , loader: 'url-loader?limit=8192' }
      , { test: /\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/
        , loader: 'url-loader?limit=10000&mimetype=application/font-woff' }
      , { test: /\.ttf(\?v=\d+\.\d+\.\d+)?$/
        , loader: 'url-loader?limit=10000&mimetype=application/octet-stream' }
      , { test: /\.eot(\?v=\d+\.\d+\.\d+)?$/
        , loader: 'file-loader' }
      , { test: /\.svg(\?v=\d+\.\d+\.\d+)?$/
        , loader: 'url-loader?limit=10000&mimetype=image/svg+xml' }

      ]
    }
  , babel: {
      optional: ['runtime', "es7.functionBind", "es7.objectRestSpread"]
    }
  , stylus: {
      use: [ (require('nib'))() ]
    }
  , plugins: [
      new webpack.HotModuleReplacementPlugin()
    , new webpack.NoErrorsPlugin()
    , new webpack.ResolverPlugin(
        new webpack.ResolverPlugin
          .DirectoryDescriptionFilePlugin('bower.json', ['main'])
      )
    , new HtmlWebpackPlugin({
        title: '申请主机-青云'
      , script: './main.js'
      })
    ]
  }
}
