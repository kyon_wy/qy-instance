import React from 'react'

export default class extends React.Component {

  render() {
    var stat = this.state.passwdStatus
      , cStat = this.state.confirmStatus
      , countStat = this.state.countStatus
      , passwdID
      , passwdClass
      , confirmID
      , confirmClass
      , countClass
      , countID
      , countHint = this.state.countHint
      , count = this.props.count
      , volumeChecked = this.props.volumeChecked
      , klass = 'btn btn-primary disabled'
      , helpClass ='help-block';

    [ passwdClass, passwdID ] = this.checkStat(stat);
    [ confirmClass, confirmID ] = this.checkStat(cStat);
    [ countClass, countID ] = this.checkStat(countStat);
    if (stat == 'nok' || countStat == 'nok')
      helpClass += ' error-hint'
    else if (stat == 'ok' && countStat == 'ok')
      helpClass += ' success-hint'

    if (volumeChecked) {
      countHint = ' (在选择加载硬盘的情况下，每次只能创建一台主机)'
    }

    if (stat == 'ok' && cStat == stat && countStat != 'nok')
      klass = 'btn btn-primary'

    if ( count == '0')
      count = ''

    return (
      <div className='basic-container'>
        <form className='basic-info'>
          <div className='form-group row'>
            <label className='col-sm-2 control-label'>主机名称</label>
            <div className='col-sm-10'>
              <input type='text' className='form-control'
                     name='instance_name'
                     value={ this.props.name }
                     onChange={ ::this.handleChange }/>
            </div>
          </div>
          <div className={ countClass }>
            <label className='col-sm-2 control-label'>主机数量</label>
            <div className='col-sm-10'>
              <input type='text' className='form-control'
                     name='count'
                     id={ countID }
                     value={ count }
                     readOnly={ volumeChecked }
                     onChange={ ::this.handleChange }/>
            </div>
          </div>
          <div className='form-group row'>
            <label className='col-sm-2 control-label'>用户名</label>
            <div className='col-sm-10'>
              <input type='text' className='form-control'
                     placeholder='root' readOnly/>
            </div>
          </div>
          <div className={ passwdClass }>
            <label className='col-sm-2 control-label'>密码</label>
            <div className='col-sm-10'>
              <input type='password' className='form-control'
                     aria-describedby='helpBlock'
                     name='login_passwd'
                     id={ passwdID }
                     ref='passwd'
                     onChange={ ::this.handleChange }/>
            </div>
          </div>
          <div className={ confirmClass }>
            <label className='col-sm-2 control-label'>确认密码</label>
            <div className='col-sm-10'>
              <input type='password' className='form-control'
                     aria-describedby='helpBlock'
                     name='confirm_passwd'
                     id={ confirmID }
                     onChange={ ::this.handleChange }/>
            </div>
          </div>
          <div className={ helpClass }>
            <span className='count-hint'>{ this.state.passwdHint }</span>
            <span className='count-hint'>{ this.state.countHint }</span>
          </div>
          <div className='form-group row'>
            <div className='col-sm-offset-2 col-sm-10'>
              <button className={ klass } onClick={ ::this.handleClick }>创建</button>
            </div>
          </div>
        </form>
      </div>
    )
  }

  constructor() {
    super()

    this.state = {
      'passwdStatus': ''
    , 'passwdHint': '密码至少 8 位, 并包括大小写字母及数字'
    , 'confirmStatus': ''
    , 'countStatus': 'ok'
    , 'countHint': ''
    }
  }

  checkStat(stat) {
    var resultID, resultClass

    if (stat.length == 0) {
      resultID = ''
      resultClass = 'form-group row'
    } else if (stat == 'ok') {
      resultID = 'inputSuccess1'
      resultClass = 'form-group row has-success'
    } else {
      resultID = 'inputError1'
      resultClass = 'has-error form-group row'
    }

    return [ resultClass, resultID ]
  }

  handleChange(e) {
    var name = e.target.name
      , val = e.target.value

    if (name == 'confirm_passwd') {
      if  (val.length != 0 && val == this.refs.passwd.value)
        this.setState({ confirmStatus: 'ok' })
      else
        this.setState({ confirmStatus: 'nok' })

      return
    }

    if (name == 'login_passwd') {
      var stat
      stat = this.checkPwd(val)
      if (stat == 'ok') {
        this.setState({ passwdStatus: 'ok', passwdHint: '' })
      } else {
        this.setState({ passwdStatus: 'nok', passwdHint: stat })
      }
    } else if (name == 'count') {
      if (Number.isInteger(parseInt(val))) {
        this.setState({ countStatus: 'ok', countHint: '' })
        val = parseInt(val)
      }
      else {
        this.setState({ countStatus: 'nok', countHint: ' 请填写有效数字' })
        if (val.length == 0)
          val = '0'
        else
          return
      }
    }

    this.props.updateConfig(name, val)
  }

  checkPwd(pwd) {
    if (pwd.length < 8)
      return ('(密码太短)')
    else if (pwd.search(/\d/) == -1)
      return ('(密码需要包含数字)')
    else if (pwd.search(/[a-z]/) == -1)
      return ('(密码需要包含字母)')
    else if (pwd.search(/[A-Z]/) == -1)
      return ('(密码需要包含大写字母)')

    return ('ok')
  }

  handleClick(e) {
    e.preventDefault()

    this.props.createInstance()
  }
}
