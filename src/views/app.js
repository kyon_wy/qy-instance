import React from 'react'

let i18n = {"create_time":"创建时间","description":"描述","image_id":" ID","image_name":"名称","os_family":"操作系统发行版","owner":"提供者ID","platform":"操作系统平台","processor_type":"支持的处理器类型. 有效值为 64 位 ( 64bit ) 和 32 位 ( 32bit )","provider":"提供者. system: 映像提供者为青云系统; self: 提供者为用户自己","recommended_type":"推荐主机配置","size":"空间大小","status":"状态. pending: 等待被创建; available: 可用状态，此时可以基于该映像创建主机; deprecated: 已被弃用，此时不再可以基于该映像创建新的主机, 但不影响已有主机的正常使用; suspended: 由于欠费，已被暂停使用; deleted: 已被删除，但处于此状态的映像在2小时之内仍可以被恢复为 available 状态; ceased: 已被彻底删除，处于此状态的映像无法恢复","status_time":"最近一次状态变更时间","transition_status":"过渡状态. creating: 创建中, 由 pending 状态变成 available 状态; suspending: 欠费暂停中, 由 available 状态变成 suspended 状态; resuming: 恢复中, 由 suspended 状态变成 available 状态; deleting: 删除中, 由 available/suspended 状态变成 deleted 状态; recovering: 恢复中, 由 deleted 状态变成 available 状态","visibility":"可见范围. public: 对所有人可见, 例如系统提供的映像; private: 只对映像所有者可见, 例如用户的自有映像"}

export default class extends React.Component {
  render() {
    return (
      <div className='main'>
      </div>
    )
  }
}
