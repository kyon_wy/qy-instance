import React from 'react'

export default class extends React.Component {

  constructor() {
    super()
  }

  render() {
    return (
      <div className='path step-container'>
        { this.props.step.map(::this.renderStep) }
      </div>
    )
  }

  renderStep(val, idx) {
    var currentStep = this.props.currentStep
      , klass = 'step'

    if (currentStep == idx+1)
      klass += ' active'
    else if (currentStep > idx+1)
      klass += ' passed'

    return (
      <div key={ idx } className={ klass }>
        <a href='#' onClick={ ::this.handleClick } id={ idx + 1 }>
          <span className='num'>{ idx + 1 }</span>
          <span className='content'>{ val }</span>
        </a>
      </div>
    )
  }

  handleClick(e) {
    e.preventDefault()

    var step = +e.target.parentElement.id

    this.props.setCurrentStep(step)
  }
}
