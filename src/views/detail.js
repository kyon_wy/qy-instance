import React from 'react'
import _ from 'underscore'

export default class extends React.Component {

  render () {
    return (
      <div className='configuration-container'>
        <h5>主机类型</h5>
        { this.props.volume.map(::this.renderVol) }
        <h5>CPU</h5>
        { this.props.cpu.map(::this.renderCPU) }
        <h5>内存</h5>
        { this.props.memory.map(::this.renderMem) }
        <h5>硬盘</h5>
        <table className='disk-table'>
          <thead>
            <tr>
              <th className='check-cell'><input type='checkbox' checked = { this.state.allChecked }
                         name='all'
                         onChange={ ::this.volumeChecked }/></th>
              <th>ID</th>
              <th>名称</th>
              <th>容量 (GB)</th>
              <th>类型</th>
            </tr>
          </thead>
          <tbody>
            { this.state.volumeInfo.map(::this.renderVolumeInfo) }
          </tbody>
        </table>
      </div>
    )
  }

  constructor() {
    super()

    this.state = {
      volumeInfo: []
    , checkedVolume: {}
    , allChecked: false
    }
  }

  componentWillMount() {
    var volumeInfo = this.props.volumeInfo
      , vol = this.props.currentVol
      , checkedVolume = this.props.checkedVolume
      , volArray
      , allChecked = false
      , currentType

    volumeInfo = volumeInfo.filter((val) => {
        return val.volume_type == vol[1]
    })

    currentType = volumeInfo.map((val) => {
       if (checkedVolume[val.volume_id] == undefined )
         checkedVolume[val.volume_id] = false
       return val.volume_id
    })

    checkedVolume = _.pick(checkedVolume, currentType)
    volArray = _.pairs(checkedVolume)

    if (volArray.length != 0) {
      allChecked = volArray.reduce((acc, val) => {
         return acc && val[1]
      }, true)
    }
    this.setState({ volumeInfo: volumeInfo, checkedVolume: checkedVolume, allChecked: allChecked })
  }

  componentWillReceiveProps(next) {
    var volumeInfo = next.volumeInfo
      , vol = next.currentVol
      , checkedVolume = next.checkedVolume
      , volArray
      , allChecked = false
      , currentType

    if (vol.length != 0) {
      volumeInfo = volumeInfo.filter((val) => {
         return val.volume_type == vol[1]
      })
    }

    currentType = volumeInfo.map((val) => {
       if (checkedVolume[val.volume_id] == undefined )
         checkedVolume[val.volume_id] = false
       return val.volume_id
    })

    checkedVolume = _.pick(checkedVolume, currentType)
    volArray = _.pairs(checkedVolume)

    if (volArray.length != 0) {
      allChecked = volArray.reduce((acc, val) => {
         return acc && val[1]
      }, true)
    }

    this.setState({ volumeInfo: volumeInfo, checkedVolume: checkedVolume, allChecked: allChecked })
  }

  renderVol(val, idx) {
    var name = 'volume_type-' + val[0]
      , text = val[1]
      , klass = 'btn'

    if (val[0] == +this.props.currentVol[0]) {
      klass += ' active'
    }

    return (
      <button className={ klass }
              key={ idx }
              name={ name }
              onClick={ ::this.handleClick }>{ text }</button>
    )
  }

  renderCPU(val, idx) {
    var name = 'cpu-' + val
      , text = val + ' 核'
      , klass = 'btn'

    if ( val == this.props.currentCPU ) {
      klass += ' active'
    }

    return (
      <button className={ klass }
              key={ idx }
              name={ name }
              onClick={ ::this.handleClick }>{ text }</button>
    )
  }

  renderMem(val, idx) {
    var name = 'memory-' + (val * 1024)
      , text = val + ' G'
      , klass = 'btn'
      , cpu = this.props.currentCPU

    if ( val * 1024 == this.props.currentMem )
      klass += ' active'
    if ( val > cpu * 4 || val < cpu / 2)
      klass += ' disabled'

    return (
      <button className={ klass }
              key={ idx }
              name={ name }
              onClick={ ::this.handleClick }>{ text }</button>
    )
  }

  renderVolumeInfo(val, idx) {
    var id = val.volume_id.toString()
      , checkedVolume = this.state.checkedVolume
      , checked

    checkedVolume[id] = checkedVolume[id] || false

    checked = checkedVolume.all || checkedVolume[id]

    return (
      <tr key={ idx }>
        <td className='check-cell'><input type='checkbox' checked={ checked }
                   name={ val.volume_id.toString() }
                   onChange={ ::this.volumeChecked }/></td>
        <td>{ val.volume_id }</td>
        <td>{ val.volume_name }</td>
        <td>{ val.size }</td>
        <td>{ val.volume_type }</td>
      </tr>
    )
  }

  volumeChecked(e) {
    var checkedVolume = this.state.checkedVolume
      , volumeInfo = this.state.volumeInfo

    if (e.target.name == 'all') {
      volumeInfo.map((val) => {
        checkedVolume[val.volume_id] = e.target.checked
      })
    } else {
      checkedVolume[e.target.name] = e.target.checked
    }

    this.props.updateConfig('checkedVolume', checkedVolume)
  }

  handleClick(e) {
    e.preventDefault()

    var name = e.target.name
      , target = name.split('-')[0]
      , val = name.split('-')[1]
      , config

    this.props.updateConfig(target, val)
  }
}
