import React from 'react'

export default class extends React.Component {

  render() {

    return (
      <div className='net-container'>
        <h5>基础网络</h5>
        <span className='hint'>提示：基础网络主机的内网 IP 可能会发生变化</span>
        <div>
          <ul>
            { ::this.renderNet(this.props.primaryNet) }
          </ul>
        </div>
        <h5>受管私有网络</h5>
        <span className='hint'>使用路由器来管理的私有网络</span>
        <div>
          <ul>
          { ::this.props.privateNet.map(::this.renderNet) }
          </ul>
        </div>
      </div>
    )
  }

  renderNet(val, idx) {
    var currentNet = this.props.currentNet
      , klass = val.id == currentNet ? 'active' : ''
      , idx = idx || 0
      , checked = klass == 'active' ? true : false

    return (
      <li key={ idx } className={ klass }>
        <input type='checkbox' name={ val.id }
               onChange={ ::this.handleChange }
               checked={ checked }/>
        <span>{ val.name }</span>
      </li>
    )
  }

  handleChange(e) {
    var netID = e.target.name
      , checked = e.target.checked

    if (checked)
      this.props.updateConfig('vxnets.1', netID)
    else
      this.props.updateConfig('vxnets.1', '')
  }
}
