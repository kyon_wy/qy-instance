import React from 'react'
import _ from 'underscore'

import constructURL from 'mods/utils'
import request from 'mods/request'

export default class extends React.Component {

  render() {
    return (
      <div className='status-container'>
        <table className='table table-bordered'>
          <thead>
            <tr>
              <th>ID</th>
              <th>名称</th>
              <th>状态</th>
              <th>映像</th>
              <th>网络</th>
              <th>公网 IP</th>
              <th>配置</th>
              <th>创建时间</th>
            </tr>
          </thead>
          <tbody>
            { this.state.instances.map(::this.renderInstance) }
          </tbody>
        </table>
        <div className='back'>
          <button className='quit' onClick={ ::this.quit }>退出</button>
          <button onClick={ ::this.back }>再申请一台</button>
        </div>
      </div>
    )
  }

  quit() {
    window.close()
  }

  back() {
    this.props.reset()
  }

  renderInstance(val, idx) {
    return (
      <tr key={ idx }>
        <td>{ val.instance_id }</td>
        <td>{ val.instance_name }</td>
        <td>{ val.status }</td>
        <td>{ val.image }</td>
        <td>{ val.vxnets }</td>
        <td>{ val.eip }</td>
        <td>{ val.instance_type }</td>
        <td>{ val.create_time }</td>
      </tr>
    )
  }

  constructor() {
    super()

    this.state = { instances: []
                 , timerID: null
                 }
  }

  componentWillMount() {
    var ids = this.props.instances

    this.describeInstance(ids)
  }

  componentWillUnmount() {
    var timer = this.state.timerID

    if (timer != null)
      clearTimeout(timer)
  }

  describeInstance(ids) {
    var key = 'instances.'
      , secret = this.props.secret
      , accessID = this.props.accessID
      , url
      , params
      , instanceSet
      , pendingIDs = []
      , required = ['instance_id', 'instance_name', 'status', 'image'
                  , 'vxnets', 'eip', 'instance_type', 'create_time']
      , ids = this.props.instances
      , timer = null
      , type

    if (ids.length == 0) {
      return
    }

    params = { 'action': 'DescribeInstances' }

    params['status.1'] = 'running'
    params['status.2'] = 'pending'

    url = constructURL(secret, accessID, params)

    request({ null, url }).then((resp) => {

      instanceSet = resp.instance_set

      instanceSet = instanceSet.map((instance) => {
        instance = _.pick(instance, required)

        if (instance.status == 'pending') {
          pendingIDs.push(instance.instance_id)
          instance.status = '创建中..'
          instance.vxnets = ''
        } else if (instance.status == 'running') {
          instance.vxnets = instance.vxnets[0].vxnet_id
          instance.status = '运行中'
        }

        if (instance.eip)
          instance.eip = instance.eip.eip_addr
        else
          instance.eip = ''

        instance.image = instance.image.os_family

        type = this.convert(instance.instance_type)
        instance.instance_type = type

        return instance
      })

      if (pendingIDs.length != 0)
        timer = setTimeout(this.describeInstance.bind(this, pendingIDs), 100)

      this.setState({ instances: instanceSet, timerID: timer })
    }, (stat) => {

    })
  }

  convert(type) {
    var cpu = type.split('m')[0].split('c')[1]
      , mem = type.split('m')[1]

    return cpu + ' 核 ' + mem + 'G'
  }


}
