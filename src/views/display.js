import React from 'react'
import _ from 'underscore'

export default class extends React.Component {

  render() {
    var config = this.props.currentConfig
      , nameMap = { 'image_name': '映像', 'volume_type': '主机类型'
                  , 'cpu': 'CPU', 'memory': '内存'
                  , 'instance_name': '名称', 'host_name': 'Hostname'
                  , 'count': '数量' }
      , display

    display = _.pick(config, _.keys(nameMap))
    display = this.rename(display, nameMap)

    display = _.pairs(display)

    return (
      <div className='display-container'>
        <span className='bulletin-title'>配置详情</span>
        <ul className='bulletin-content'>
          { display.map(::this.renderItem) }
        </ul>
      </div>
    )
  }

  rename(obj, newNames) {
    var omit = _.keys(newNames)

    omit.unshift(obj)

    return _.reduce(newNames, (o, nu, old) => {
      if (_.has(obj, old)) {
        o[nu] = obj[old]
        return o
      } else
        return o
    }
    , _.omit.apply(null, omit))
  }

  renderItem(val, idx) {
    var unit = ''

    if (val[0] == 'CPU')
      unit = ' 核'
    else if (val[0] == '内存') {
      unit = 'G'
      val[1] = val[1] / 1024
    } else if (val[0] == '主机类型') {
      val[1] = val[1][1] || ''
    }

    return (
      <li key={ idx }>
        <span className='config-key'>{ val[0] }</span>
        <span className='config-val'>{ val[1] + unit }</span>
      </li>
    )
  }
}
