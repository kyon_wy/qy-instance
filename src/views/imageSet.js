import React from 'react'
import _ from 'underscore'
import Immutable from 'immutable'

import Step from 'views/step'
import Raw from 'views/raw'
import Display from 'views/display'
import Detail from 'views/detail'
import Net from 'views/net'
import Basic from 'views/basic'
import CreatedInstances from 'views/status'

import constructURL from 'mods/utils'
import request from 'mods/request'

export default class extends React.Component {

  constructor() {
    super()

    this.state = {
      currentStep: 1
    , steps: ['选择映像', '配置选择', '网络设置', '基本信息']
    , allOS: []
    , allImage: []
    , allCPU: [1, 2, 4, 8, 12]
    , allMem: [1, 2, 4, 6, 8, 12, 16, 24, 32, 40, 48]
    , allVolume: [[0, '性能型'], [2, '容量型'], [3, '超高性能型']]
    , volumeInfo: []
    , allNet: []
    , currentConfig: {
        'image_id': ''
      , 'image_name': ''
      , 'instance_type': ''
      , 'instance_class': ''
      , 'cpu': ''
      , 'memory': ''
      , 'volume_type': [0, '性能型']
      , 'checkedVolume': {}
      , 'vxnets.1': 'vxnet-0'
      , 'login_mode': 'passwd'
      , 'instance_name': ''
      , 'count': '1'
      , 'login_passwd': ''
      , 'host_name': ''
      }
    , createdInstances: []
    }
  }

  componentWillMount() {
    var allOS
      , allImage
      , imageSet = this.props.imageSet

    allOS = imageSet.map((val, idx) => {
       return val.os_family
    })
    allOS.unshift('all')
    allOS = _.uniq(allOS)

    allImage = imageSet.map((val, idx) => {
       return { name: val.image_name, id: val.image_id, os: val.os_family }
    })

    this.fillConfig(imageSet[0].image_id)
    this.setState({ imageSet: imageSet
                  , allOS: allOS
                  , allImage: allImage})
  }

  render() {
    var content = null
      , step = null
      , currentStep = this.state.currentStep
      , currentConfig = this.state.currentConfig
      , allNet = this.state.allNet
      , primaryNet = allNet.filter((val) => {
         return val.id == 'vxnet-0'
      })[0]
      , privateNet = _.without(allNet, primaryNet)
      , volumeChecked = false
      , createdInstances = this.state.createdInstances

    if (createdInstances.length != 0) {
      return (
        <CreatedInstances secret = { this.props.secretKey }
                          accessID = { this.props.accessID }
                          reset = { ::this.reset }
                          instances = { createdInstances }/>
      )
    }

    for (var key in currentConfig.checkedVolume) {
      if (currentConfig.checkedVolume[key]) {
        volumeChecked = true
        break
      }
    }

    if (allNet.length == 0) {
      primaryNet = { 'name': '基础网络', 'id': 'vxnet-0' }
    }

    switch (currentStep) {
      case 1:
        content = <Raw allOS={ this.state.allOS }
                       allImage={ this.state.allImage }
                       selectedImage={ currentConfig.image_name }
                       fillConfig = { ::this.fillConfig }/>
        break
      case 2:
        content = <Detail volume={ this.state.allVolume }
                          cpu={ this.state.allCPU }
                          memory={ this.state.allMem }
                          currentCPU={ currentConfig.cpu }
                          currentMem={ currentConfig.memory }
                          currentVol={ currentConfig.volume_type }
                          checkedVolume={ currentConfig.checkedVolume }
                          volumeInfo={ this.state.volumeInfo }
                          updateConfig={ ::this.updateConfig }/>
        break
      case 3:
        content = <Net primaryNet={ primaryNet }
                       privateNet={ privateNet }
                       currentNet={ currentConfig['vxnets.1'] }
                       updateConfig={ ::this.updateConfig }/>
        break
      case 4:
        content = <Basic name={ currentConfig.instance_name }
                         count={ currentConfig.count }
                         pwd={ currentConfig.login_passwd }
                         volumeChecked={ volumeChecked }
                         createInstance={ ::this.createInstance }
                         updateConfig={ ::this.updateConfig }/>
        break
      default:
        break
    }

    return (
      <div className="container">
        <div className='input-field'>
          <Step currentStep={ this.state.currentStep }
                step={ this.state.steps }
                setCurrentStep={ ::this.setCurrentStep }/>
          { content }
        </div>
        <div className='bulletin-board'>
          <Display currentConfig={ currentConfig }/>
        </div>
      </div>
    )
  }

  reset() {
    this.setState({ 'createdInstances': [], 'currentStep': 1 })
  }

  updateConfig(target, val) {
    var config = Immutable.Map(this.state.currentConfig)

    if (target == 'cpu')
      config = config.set('memory', val * 1024)

    if (target == 'volume_type') {
      val = this.state.allVolume.filter((type) => {
         return type[0] == +val
      })[0]
    }

    config = config.set(target, val).toJS()

    this.setState({ currentConfig: config })
  }

  fillConfig(id) {
    var image
      , config
      , type
      , cpu
      , memory
      , id
      , name

    image = this.props.imageSet.filter((val) => {
       return val.image_id == id
    })[0]

    id = image.image_id
    name = image.image_name
    type = image.recommended_type
    cpu = type.split('m')[0].split('c')[1]
    memory = type.split('m')[1] * 1024

    config = Immutable.Map(this.state.currentConfig)
    config = config.set('image_id', id)
    config = config.set('image_name', name)
    config = config.set('instance_type', type)
    config = config.set('instance_class', '0')
    config = config.set('cpu', cpu)
    config = config.set('memory', memory)
    config = config.set('vxnets.1', 'vxnet-0')

    config = config.toJS()

    this.setState({ currentConfig: config })
  }

  checkVolume() {
    var params = {
        'action': 'DescribeVolumes'
      , 'status': 'available'
      }
    , secret = this.props.secretKey
    , accessID = this.props.accessID
    , url = constructURL(secret, accessID, params)
    , volumeInfo
    , key = ['volume_id', 'volume_name', 'size', 'volume_type']
    , vol = []
    , allVol = this.state.allVolume
    , config = Immutable.Map(this.state.currentConfig).toJS()

    request({ xhr: null, url: url}).then((resp) => {

      volumeInfo = resp.volume_set.map((val) => {
        var volume = _.pick(val, key)
        vol[0] = +volume.volume_type

        volume.volume_type = allVol.filter((v) => {
          return v[0] == vol[0]
        })[0][1]

        vol[1] = volume.volume_type

        return volume
      })

      this.setState({ volumeInfo: volumeInfo, currentConfig: config })
    }, (stat) => {
      alarm(stat)
    })
  }

  checkNet() {
    var allNet = this.state.allNet
      , config = Immutable.Map(this.state.currentConfig)
      , net = config.get('vxnets.1') || 'vxnet-0'

    if (allNet.length == 0) {
      var params = {
          'action': 'DescribeVxnets'
        }
      , secret = this.props.secretKey
      , accessID = this.props.accessID
      , url = constructURL(secret, accessID, params)
      , allNet = this.state.allNet
      , netInfo

      request({ null, url}).then((resp) => {

        allNet = resp.vxnet_set.map((val) => {
          if (val.vxnet_id == 'vxnet-0')
            netInfo = { 'id': val.vxnet_id, 'name': '基础网络' }
          else
            netInfo = { 'id': val.vxnet_id, 'name': val.vxnet_name }

          return netInfo
        })
        this.setState({ allNet: allNet })
      }, (stat) => {
        alarm(stat)
      })
    }

    config = config.set('vxnets.1', net).toJS()
    this.setState({ currentConfig: config })
  }

  setCurrentStep(step) {
    if (step == 2)
      this.checkVolume()
    else if (step == 3)
      this.checkNet()
    this.setState({ currentStep: step })
  }

  createInstance() {
    var config = Immutable.Map(this.state.currentConfig).toJS()
      , params = { 'action': 'RunInstances' }
      , volumes = []
      , url
      , secret = this.props.secretKey
      , accessID = this.props.accessID

    for (var key in config.checkedVolume) {
      if (config.checkedVolume[key])
        volumes.push(key)
    }

    params['instance_class'] = config.volume_type[0]
    delete(config['volume_type'])
    delete(config['checkedVolume'])

    volumes.map((val, idx) => {
      var key = 'volumes.' + (idx + 1)
      params[key] = val
    })

    Object.assign(params, config)

    url = constructURL(secret, accessID, params)

    request({null, url}).then((resp) => {
      this.setState({ createdInstances: resp.instances})
    }, (stat) => {
      alarm('create failed')
    })
  }
}
