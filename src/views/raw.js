import React from 'react'
import _ from 'underscore'

export default class extends React.Component {

  constructor() {
    super()

    this.state = {
        page: 1
      , total: 1
      , perPage: 8
      , inScreenImage: []
      , selectedOS: 'all'
    }
  }

  componentWillMount() {
    var os = this.props.allOS

    this.setState({
      total: Math.ceil(this.props.allImage.length / this.state.perPage)
    , inScreenImage: this.props.allImage
    })
  }

  render() {
    var image = this.state.inScreenImage
      , page = this.state.page
      , total = this.state.total
      , start = (page - 1) * this.state.perPage
      , end = page * this.state.perPage
      , pagination = null
      , content = null

    if (total > 1) {
      pagination = <Pagination page={ page } total={ total }
                    pageTo={ ::this.updatePage }/>
    }

    return (
      <div className="raw-info">
        <div className="os-dist-container">
          <div>
            <span>操作系统:</span>
            <ul className="os-dist">
              { this.props.allOS.map(::this.renderTab) }
            </ul>
          </div>
          { pagination }
        </div>
        <div className="image-name-container">
          <table className="os-info">
            <tbody>
              { image.slice(start, end).map(::this.renderItem) }
            </tbody>
          </table>
        </div>
      </div>
    )
  }

  renderItem(val, idx) {
    var title = val.name
      , id = val.id
      , klass = title == this.props.selectedImage ? 'active' : ''

    return (
      <tr className={ klass } id={ id } key={ idx } onClick={ ::this.handleItemClick }>
        <td className='os-name'>{ title }</td>
        <td className='os-id'>ID: { id }</td>
      </tr>
    )
  }

  handleItemClick(e) {
    var id = e.target.parentElement.id

    // show detailed information and configuration
    this.props.fillConfig(id)
  }

  updatePage(page) {
    this.setState({ page: page })
  }

  renderTab(val, idx) {
    var klass = 'os-dist-picker'

    if (val == this.state.selectedOS) {
      klass += ' picked'
    } else {
      klass += ''
    }

    return (
      <li key={ idx }>
        <button className={ klass } onClick={ ::this.handleTabClick }>
          { val }
        </button>
      </li>
    )
  }

  handleTabClick(e) {
    e.preventDefault()
    var os = e.target.textContent
      , selected

    if (os == 'all') {
      selected = this.props.allImage
    } else {
      selected = this.props.allImage.filter((val) => {
         return  val.os == os
      })
    }

    this.setState({ selectedOS: os
                  , page: 1
                  , inScreenImage: selected
                  , total: Math.ceil(selected.length / this.state.perPage)
                  })
  }

}

class Pagination extends React.Component {
  render() {
    var indices = _.range(1, this.props.total + 1)
      , pKlass = this.props.page == 1 ? 'disabled' : ''
      , nKlass = this.props.page == this.props.total ? 'disabled' : ''
    return (
      <div className='pagination'>
        <ul>
          <li className={ pKlass }>
            <a href='#' aria-label='Previous'
                        onClick={ ::this.previousPage }>
              <span aria-hidden='true'>&laquo;</span>
            </a>
          </li>
          { indices.map(::this.renderItem) }
          <li className={ nKlass }>
            <a href='#' aria-label='Next'
                        onClick={ ::this.nextPage }>
              <span aria-hidden='true'>&raquo;</span>
            </a>
          </li>
        </ul>
      </div>
    )
  }

  renderItem(val, idx) {
    var klass = val == this.props.page ? 'active' : ''

    return (
      <li className={ klass } key={ idx }>
        <a href='#' onClick={ ::this.handleClick }>{ val }</a>
      </li>
    )
  }

  handleClick(e) {
    e.preventDefault()
    var page = +e.target.text

    this.pageTo(page)
  }

  previousPage() {
    if (this.props.page == 1)  return
    this.pageTo(this.props.page - 1)
  }

  nextPage() {
    if (this.props.page == this.props.total)  return
    this.pageTo(this.props.page + 1)
  }

  pageTo(page) {
    this.props.pageTo(page)
  }
}
