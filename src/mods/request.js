var request = ({
  xhr
  , url
  , method = 'GET'
  , responseType = 'json' }) => {

  xhr = xhr || new XMLHttpRequest()

  return new Promise((resolve, reject) => {
    xhr.open(method, url, true)
    xhr.responseType = responseType
    xhr.addEventListener('load', function() {
      if (xhr.status >= 200 && xhr.status < 300) {
        resolve(xhr.response)
      } else {
        reject(xhr.status)
      }
    })
    xhr.send()
  })
}

export default request
