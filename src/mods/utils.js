function makeISOString(d) {
  function pad(number) {
    if (number < 10) {
      return '0' + number
    }
    return number
  }
  return (d.getUTCFullYear()
    + '-' + pad(d.getUTCMonth() + 1)
    + '-' + pad(d.getUTCDate())
    + 'T' + pad(d.getUTCHours())
    + ':' + pad(d.getUTCMinutes())
    + ':' + pad(d.getUTCSeconds())
    + 'Z')
}

var constructURL = (secret_key, access_id, options) => {
  var ts = makeISOString(new Date())
    , crypto = require('crypto')
    , params = {}
    , signature
    , uri = ''
    , prefix = 'https://api.qingcloud.com/iaas/?'
    , commons = {
        "access_key_id": access_id,
        "signature_method":"HmacSHA256",
        "signature_version":1,
        "time_stamp": ts,
        "version":1,
        "zone":"gd1"
      }

  Object.assign(params, commons, options)
  //XXX(kyon): 'instances.10 < instances.2'
  Object.keys(params).sort().forEach((key) => {
    uri += (key + '=' + encodeURIComponent(params[key]) + '&')
  })
  uri = uri.slice(0, -1)

  signature = crypto.createHmac('sha256', secret_key)
              .update('GET\n/iaas/\n' + uri).digest('base64').trim()

  uri += ('&signature=' + encodeURIComponent(signature))

  return (prefix + uri)
}

export default constructURL
