import 'normalize.css'
import './styles/index.styl'

import $ from 'jquery'
import React from 'react'
import ReactDom from 'react-dom'
import constructURL from 'mods/utils'
import request from 'mods/request'
import ImageSet from 'views/imageSet'

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      showResult: false
    , accessID: ''
    , secretKey: ''
    , imageSet: {}
    }
  }

  handleChange(e) {
    e.preventDefault()
    var newState = {}

    newState[e.target.name] = e.target.value
    this.setState(newState)
  }

  handleSubmit(e) {
    e.preventDefault()
    var params = {
        'action':   'DescribeImages'
      , 'provider': 'system'
      , 'status':   'available'
      , 'platform': 'linux'
      }
    , secret = this.state.secretKey
    , accessID = this.state.accessID
    , url = constructURL(secret, accessID, params)

    request({ null, url }).then((resp) => {
      //hide self, handle response
      this.setState({ showResult: true, imageSet: resp.image_set })
    }, (stat) => {
      alarm(stat)
    })
  }

  getOS(val, idx) {
     val.os_family
  }

  render() {
    var content = null
    , linuxImages

    if (this.state.showResult) {
      linuxImages = this.state.imageSet.filter((val) => {
         return val.platform == 'linux'
      })

      content = <ImageSet imageSet={ linuxImages }
                 secretKey={ this.state.secretKey }
                 accessID={ this.state.accessID }/>
    } else {
      content = (
        <div className="form-container">
          <form className="secret" onSubmit={ ::this.handleSubmit }>
            <span>查询可申请映像详细信息</span>
            <div className="form-group">
              <input onChange={ ::this.handleChange } autoFocus="true"
                     name="accessID"
                     placeholder="access key id"/>
            </div>
            <div className="form-group">
              <input onChange={ ::this.handleChange }
                     name="secretKey"
                     placeholder="secret access key" />
            </div>
            <div className="form-group">
              <button className="btn btn-primary">提交</button>
            </div>
          </form>
        </div>
      )
    }

    return content
  }
}

$('document').on('click', 'a[href="#"]', (e) => {
   e.preventDefault()
})

var appViewWrapper = $('<div>', { 'class': 'wrapper' })

$('body').prepend(appViewWrapper)

ReactDom.render(<App />, appViewWrapper[0])
